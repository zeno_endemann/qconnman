#ifndef _WIREDPAGE_H
#define _WIREDPAGE_H

#include "ui_wiredpage.h"

class WiredPage: public QWidget,
                 private Ui::WiredPage
{
    Q_OBJECT
public:
    WiredPage(const QModelIndex &technology, QWidget *parent = 0);
    ~WiredPage();

/*
private slots:
    void updateButtonsVisibility();

    void toggleTechnology(bool checked);

    void connect();
    void disconnect();

private:
    Ui::WiredPage ui;
    */
};

#endif
