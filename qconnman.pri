QCONNMAN_VERSION = 1.21.0

isEmpty(QCONNMAN_LIBRARY_TYPE) {
    QCONNMAN_LIBRARY_TYPE = shared
}

QT += dbus
QCONNMAN_INCLUDEPATH = $${PWD}/src
QCONNMAN_LIBS = -lqconnman
contains(QCONNMAN_LIBRARY_TYPE, staticlib) {
    DEFINES += QCONNMAN_STATIC
} else {
    DEFINES += QCONNMAN_SHARED
}

isEmpty(PREFIX) {
    PREFIX = /usr/local
}
isEmpty(LIBDIR) {
    LIBDIR = lib
}
