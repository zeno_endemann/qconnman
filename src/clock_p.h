#ifndef CLOCK_P_H
#define CLOCK_P_H

class NetConnmanClockInterface;
class ClockPrivate
{
public:
    ClockPrivate()
        : time(0)
    {
    }

    NetConnmanClockInterface *clockInterface;

    quint64 time;
    QString timeUpdates;
    QString timezone;
    QString timezoneUpdates;
    QStringList timeservers;
};

#endif
