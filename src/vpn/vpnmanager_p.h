#ifndef VPNMANAGER_P_H
#define VPNMANAGER_P_H

#include <QDBusObjectPath>

class QDBusPendingCallWatcher;
class QDBusServiceWatcher;
class NetConnmanVpnManagerInterface;
class VpnConnection;
class VpnManager;
class VpnManagerPrivate
{
public:
    explicit VpnManagerPrivate(VpnManager *parent)
        : managerInterface(0),
          connmanVpnWatcher(0),
          q_ptr(parent)
    {
    }

    int indexOfConnection(const QDBusObjectPath &path) const;

    // private slots
    void connmanVpnRegistered();
    void connmanVpnUnregistered();
    void getConnectionsResponse(QDBusPendingCallWatcher *call);

    void connectionAdded(const QDBusObjectPath &path, const QVariantMap &properties);
    void connectionRemoved(const QDBusObjectPath &path);

    NetConnmanVpnManagerInterface *managerInterface;
    QDBusServiceWatcher *connmanVpnWatcher;

    QList<VpnConnection*> connections;

    VpnManager * const q_ptr;
    Q_DECLARE_PUBLIC(VpnManager)
};

#endif
