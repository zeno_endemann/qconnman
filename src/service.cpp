/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "qconnman_debug.h"

#include "interfaces/service_interface.h"

#include "service_p.h"
#include "service.h"

ConfigurableObject::ConfigurableObject(ConnManObject *parent)
    : QObject(parent)
{
}

void ConfigurableObject::apply()
{
    Service *service = qobject_cast<Service*>(parent());
    if (!service) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid parent";
        return;
    }

    QString propertyName(objectName());
    if (!propertyName.endsWith(QLatin1String("Configuration"))) {
        qConnmanDebug() << Q_FUNC_INFO << "object is not configurable";
        return;
    }

    QVariantMap data;
    const QMetaObject *mo = metaObject();
    for (int i = mo->propertyOffset(); i < mo->propertyCount(); i++) {
        QMetaProperty mp = mo->property(i);
        QVariant propertyData = mp.read(this);
        if (propertyData.isValid() && !propertyData.isNull())
            data.insert(mp.name(), propertyData);
    }

    service->d_ptr->setConnmanProperty(propertyName, data);
}

int ServicePrivate::s_configurableObjectMetaTypeId =
    qRegisterMetaType<ConfigurableObject*>("ConfigurableObject*");
int ServicePrivate::s_ipv4DataMetaTypeId = qRegisterMetaType<IPV4Data*>("IPV4Data*");
int ServicePrivate::s_ipv6DataMetaTypeId = qRegisterMetaType<IPV6Data*>("IPV6Data*");
int ServicePrivate::s_ethernetDataMetaTypeId = qRegisterMetaType<EthernetData*>("EthernetData*");
int ServicePrivate::s_providerDataMetaTypeId = qRegisterMetaType<ProviderData*>("ProviderData*");
int ServicePrivate::s_proxyDataMetaTypeId = qRegisterMetaType<ProxyData*>("ProxyData*");
Service::Service(const ObjectPropertyData &info, QObject *parent)
    : ConnManObject(parent),
      d_ptr(new ServicePrivate(info, this))
{
    d_ptr->initialize(info);
}

Service::Service(ServicePrivate *dd, QObject *parent)
    : ConnManObject(parent),
      d_ptr(dd)
{
}

void ServicePrivate::initialize(const ObjectPropertyData &info)
{
    Q_Q(Service);
    // complex properties
    ethernet = new EthernetData(q);
    ethernet->setObjectName("Ethernet");

    ipv4 = new IPV4Data(q);
    ipv4->setObjectName("IPv4");
    ipv4Configuration = new IPV4Data(q);
    ipv4Configuration->setObjectName("IPv4.Configuration");

    ipv6 = new IPV6Data(q);
    ipv6->setObjectName("IPv6");
    ipv6Configuration = new IPV6Data(q);
    ipv6Configuration->setObjectName("IPv6.Configuration");

    proxy = new ProxyData(q);
    proxy->setObjectName("Proxy");
    proxyConfiguration = new ProxyData(q);
    proxyConfiguration->setObjectName("Proxy.Configuration");

    provider = new ProviderData(q);
    provider->setObjectName("Provider");

    serviceInterface =
        new NetConnmanServiceInterface("net.connman", objectPath.path(), QDBusConnection::systemBus(), q);
    if (!serviceInterface->isValid()) {
        qConnmanDebug() << "unable to connect to service at path: " << objectPath.path();
        return;
    }

    QObject::connect(serviceInterface, SIGNAL(PropertyChanged(QString,QDBusVariant)),
                                    q, SLOT(propertyChanged(QString,QDBusVariant)));

    // set the initial properties
    foreach (QString property, info.properties.keys())
        q->propertyChanged(property, QDBusVariant(info.properties.value(property)));
}

bool ServicePrivate::setConnmanProperty(const QString &property, const QVariant &value)
{
    QDBusPendingReply<> reply =
        serviceInterface->SetProperty(property, QDBusVariant(value));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "could not set property '" << property << "': "
                        << reply.error().message();
        return false;
    }

    return true;
}

Service::~Service()
{
}

QString Service::stateInternal() const
{
    Q_D(const Service);
    return d->state;
}

QDBusObjectPath Service::objectPath() const
{
    Q_D(const Service);
    return d->objectPath;
}

Service::ServiceState Service::state() const
{
    Q_D(const Service);
    if (d->state == QLatin1String("idle"))
        return IdleState;
    if (d->state == QLatin1String("failure"))
        return FailureState;
    if (d->state == QLatin1String("association"))
        return  AssociationState;
    if (d->state == QLatin1String("configuration"))
        return ConfigurationState;
    if (d->state == QLatin1String("ready"))
        return ReadyState;
    if (d->state == QLatin1String("disconnect"))
        return DisconnectState;
    if (d->state == QLatin1String("online"))
        return OnlineState;
    return UndefinedState;
}

void Service::setStateInternal(const QString &state)
{
    Q_D(Service);
    d->state = state;
    Q_EMIT dataChanged();
    Q_EMIT stateChanged();
    if ( d->state == QLatin1String("online")) {
        Q_EMIT connected();
    }
}

QString Service::error() const
{
    Q_D(const Service);
    return d->error;
}

void Service::setErrorInternal(const QString &error)
{
    Q_D(Service);
    d->error = error;
    Q_EMIT dataChanged();
}

QString Service::name() const
{
    Q_D(const Service);
    return d->name;
}

void Service::setNameInternal(const QString &name)
{
    Q_D(Service);
    d->name = name;
    Q_EMIT dataChanged();
}

QString Service::type() const
{
    Q_D(const Service);
    return d->type;
}

void Service::setTypeInternal(const QString &type)
{
    Q_D(Service);
    d->type = type;
    Q_EMIT dataChanged();
}

QStringList Service::security() const
{
    Q_D(const Service);
    return d->security;
}

void Service::setSecurityInternal(const QStringList &security)
{
    Q_D(Service);
    d->security = security;
    Q_EMIT dataChanged();
}

quint8 Service::strength() const
{
    Q_D(const Service);
    return d->strength;
}

void Service::setStrengthInternal(quint8 strength)
{
    Q_D(Service);
    d->strength = strength;
    Q_EMIT dataChanged();
}

bool Service::isFavorite() const
{
    Q_D(const Service);
    return d->favorite;
}

void Service::setFavoriteInternal(bool favorite)
{
    Q_D(Service);
    d->favorite = favorite;
    Q_EMIT dataChanged();
}

bool Service::isImmutable() const
{
    Q_D(const Service);
    return d->immutable;
}

void Service::setImmutableInternal(bool immutable)
{
    Q_D(Service);
    d->immutable = immutable;
    Q_EMIT dataChanged();
}

bool Service::isAutoConnect() const
{
    Q_D(const Service);
    return d->autoConnect;
}

void Service::setAutoConnectInternal(bool autoConnect)
{
    Q_D(Service);
    d->autoConnect = autoConnect;
    Q_EMIT dataChanged();
}

void Service::setAutoConnect(bool autoConnect)
{
    Q_D(Service);
    if (d->setConnmanProperty("AutoConnect", autoConnect))
        d->autoConnect = autoConnect;
}

bool Service::isRoaming() const
{
    Q_D(const Service);
    return d->roaming;
}

void Service::setRoamingInternal(bool roaming)
{
    Q_D(Service);
    d->roaming = roaming;
    Q_EMIT dataChanged();
}

QStringList Service::nameservers() const
{
    Q_D(const Service);
    return d->nameservers;
}

void Service::setNameserversInternal(const QStringList &servers)
{
    Q_D(Service);
    d->nameservers = servers;
    Q_EMIT dataChanged();
}

QStringList Service::nameserversConfiguration() const
{
    Q_D(const Service);
    return d->nameserversConfiguration;
}

void Service::setNameserversConfigurationInternal(const QStringList &nameServers)
{
    Q_D(Service);
    d->nameserversConfiguration = nameServers;
    Q_EMIT dataChanged();
}

void Service::setNameserversConfiguration(const QStringList &nameServers)
{
    Q_D(Service);
    d->setConnmanProperty("Nameservers.Configuration", nameServers);
}

QStringList Service::timeservers() const
{
    Q_D(const Service);
    return d->timeservers;
}

void Service::setTimeserversInternal(const QStringList &servers)
{
    Q_D(Service);
    d->timeservers = servers;
    Q_EMIT dataChanged();
}

QStringList Service::timeserversConfiguration() const
{
    Q_D(const Service);
    return d->timeserversConfiguration;
}

void Service::setTimeserversConfigurationInternal(const QStringList &timeServers)
{
    Q_D(Service);
    d->timeserversConfiguration = timeServers;
    Q_EMIT dataChanged();
}

void Service::setTimeserversConfiguration(const QStringList &timeServers)
{
    Q_D(Service);
    d->setConnmanProperty("Timeservers.Configuration",  timeServers);
}

QStringList Service::domains() const
{
    Q_D(const Service);
    return d->domains;
}

void Service::setDomainsInternal(const QStringList &domains)
{
    Q_D(Service);
    d->domains = domains;
    Q_EMIT dataChanged();
}

QStringList Service::domainsConfiguration() const
{
    Q_D(const Service);
    return d->domainsConfiguration;
}

void Service::setDomainsConfigurationInternal(const QStringList &domains)
{
    Q_D(Service);
    d->domainsConfiguration = domains;
    Q_EMIT dataChanged();
}

void Service::setDomainsConfiguration(const QStringList &domains)
{
    Q_D(Service);
    d->setConnmanProperty("Domains.Configuration", domains);
}

EthernetData *Service::ethernet() const
{
    Q_D(const Service);
    return d->ethernet;
}

IPV4Data *Service::ipv4() const
{
    Q_D(const Service);
    return d->ipv4;
}

IPV4Data *Service::ipv4Configuration() const
{
    Q_D(const Service);
    return d->ipv4Configuration;
}

IPV6Data *Service::ipv6() const
{
    Q_D(const Service);
    return d->ipv6;
}

IPV6Data *Service::ipv6Configuration() const
{
    Q_D(const Service);
    return d->ipv6Configuration;
}

ProxyData *Service::proxy() const
{
    Q_D(const Service);
    return d->proxy;
}

ProxyData *Service::proxyConfiguration() const
{
    Q_D(const Service);
    return d->proxyConfiguration;
}

ProviderData *Service::provider() const
{
    Q_D(const Service);
    return d->provider;
}

void Service::connect()
{
    Q_D(Service);
    d->serviceInterface->Connect();
}

void Service::disconnect()
{
    Q_D(Service);
    d->serviceInterface->Disconnect();
}

void Service::remove()
{
    Q_D(Service);
    d->serviceInterface->Remove();
}

void Service::moveBefore(Service *service)
{
    Q_D(Service);
    d->serviceInterface->MoveBefore(service->objectPath());
}

void Service::moveAfter(Service *service)
{
    Q_D(Service);
    d->serviceInterface->MoveAfter(service->objectPath());
}

void Service::resetCounters()
{
    Q_D(Service);
    d->serviceInterface->ResetCounters();
}

WifiService::WifiService(const ObjectPropertyData &info, QObject *parent)
    : Service(new WifiServicePrivate(info, this), parent)
{
    d_ptr->initialize(info);
}

WifiService::~WifiService()
{
}

QString WifiService::eap() const
{
    Q_D(const WifiService);
    return d->eapMethod;
}

void WifiService::setEap(const QString &method)
{
    Q_D(WifiService);
    d->setConnmanProperty("EAP", method);
}

QString WifiService::caCertificateFile() const
{
    Q_D(const WifiService);
    return d->caCertificateFile;
}

void WifiService::setCaCertificateFile(const QString &path)
{
    Q_D(WifiService);
    d->setConnmanProperty("CACertFile", path);
}

QString WifiService::clientCertificateFile() const
{
    Q_D(const WifiService);
    return d->clientCertificateFile;
}

void WifiService::setClientCertificateFile(const QString &path)
{
    Q_D(WifiService);
    d->setConnmanProperty("ClientCertFile", path);
}

QString WifiService::privateKeyFile() const
{
    Q_D(const WifiService);
    return d->privateKeyFile;
}

void WifiService::setPrivateKeyFile(const QString &path)
{
    Q_D(WifiService);
    d->setConnmanProperty("PrivateKeyFile", path);
}

QString WifiService::privateKeyPassphrase() const
{
    Q_D(const WifiService);
    return d->privateKeyPassphrase;
}

void WifiService::setPrivateKeyPassphrase(const QString &passphrase)
{
    Q_D(WifiService);
    d->setConnmanProperty("PrivateKeyPassphrase", passphrase);
}

QString WifiService::privateKeyPassphraseType() const
{
    Q_D(const WifiService);
    return d->privateKeyPassphraseType;
}

void WifiService::setPrivateKeyPassphraseType(const QString &type)
{
    Q_D(WifiService);
    d->setConnmanProperty("PrivateKeyPassphraseType", type);
}

QString WifiService::identity() const
{
    Q_D(const WifiService);
    return d->identity;
}

void WifiService::setIdentity(const QString &identity)
{
    Q_D(WifiService);
    d->setConnmanProperty("Identity", identity);
}

QString WifiService::phase2() const
{
    Q_D(const WifiService);
    return d->phase2;
}

void WifiService::setPhase2(const QString &phase2)
{
    Q_D(WifiService);
    d->setConnmanProperty("Phase2", phase2);
}

QString WifiService::passphrase() const
{
    Q_D(const WifiService);
    return d->passphrase;
}

void WifiService::setPassphrase(const QString &passphrase)
{
    Q_D(WifiService);
    d->setConnmanProperty("Passphrase", passphrase);
}

bool WifiService::isHidden() const
{
    Q_D(const WifiService);
    return d->hidden;
}

void WifiService::setEapInternal(const QString &method)
{
    Q_D(WifiService);
    d->eapMethod = method;
    Q_EMIT dataChanged();
}

void WifiService::setCaCertificateFileInternal(const QString &path)
{
    Q_D(WifiService);
    d->caCertificateFile = path;
    Q_EMIT dataChanged();
}

void WifiService::setClientCertificateFileInternal(const QString &path)
{
    Q_D(WifiService);
    d->clientCertificateFile = path;
    Q_EMIT dataChanged();
}

void WifiService::setPrivateKeyFileInternal(const QString &path)
{
    Q_D(WifiService);
    d->privateKeyFile = path;
    Q_EMIT dataChanged();
}

void WifiService::setPrivateKeyPassphraseInternal(const QString &passphrase)
{
    Q_D(WifiService);
    d->privateKeyPassphrase = passphrase;
    Q_EMIT dataChanged();
}

void WifiService::setPrivateKeyPassphraseTypeInternal(const QString &type)
{
    Q_D(WifiService);
    d->privateKeyPassphraseType = type;
    Q_EMIT dataChanged();
}

void WifiService::setIdentityInternal(const QString &identity)
{
    Q_D(WifiService);
    d->identity = identity;
    Q_EMIT dataChanged();
}

void WifiService::setPhase2Internal(const QString &phase2)
{
    Q_D(WifiService);
    d->phase2 = phase2;
    Q_EMIT dataChanged();
}

void WifiService::setPassphraseInternal(const QString &passphrase)
{
    Q_D(WifiService);
    d->passphrase = passphrase;
    Q_EMIT dataChanged();
}

void WifiService::setHiddenInternal(bool hidden)
{
    Q_D(WifiService);
    d->hidden = hidden;
    Q_EMIT dataChanged();
}

EthernetData::EthernetData(ConnManObject *parent)
    : ConfigurableObject(parent),
      d_ptr(new EthernetDataPrivate)
{
}

EthernetData::~EthernetData()
{
}

QString EthernetData::method() const
{
    Q_D(const EthernetData);
    return d->method;
}

void EthernetData::setMethod(const QString &method)
{
    Q_D(EthernetData);
    d->method = method;
}

QString EthernetData::interface() const
{
    Q_D(const EthernetData);
    return d->interface;
}

void EthernetData::setInterface(const QString &interface)
{
    Q_D(EthernetData);
    d->interface = interface;
}

QString EthernetData::address() const
{
    Q_D(const EthernetData);
    return d->address;
}

void EthernetData::setAddress(const QString &address)
{
    Q_D(EthernetData);
    d->address = address;
}

quint16 EthernetData::mtu() const
{
    Q_D(const EthernetData);
    return d->mtu;
}

void EthernetData::setMtu(quint16 mtu)
{
    Q_D(EthernetData);
    d->mtu = mtu;
}

quint16 EthernetData::speed() const
{
    Q_D(const EthernetData);
    return d->speed;
}

void EthernetData::setSpeed(quint16 speed)
{
    Q_D(EthernetData);
    d->speed = speed;
}

QString EthernetData::duplex() const
{
    Q_D(const EthernetData);
    return d->duplex;
}

void EthernetData::setDuplex(const QString &duplex)
{
    Q_D(EthernetData);
    d->duplex = duplex;
}

IPV4Data::IPV4Data(ConnManObject *parent)
    : ConfigurableObject(parent),
      d_ptr(new IPV4DataPrivate)
{
}

IPV4Data::~IPV4Data()
{
}

QString IPV4Data::method() const
{
    Q_D(const IPV4Data);
    return d->method;
}

void IPV4Data::setMethod(const QString &method)
{
    Q_D(IPV4Data);
    d->method = method;

    if (objectName() == "IPv4.Configuration" && d->method == QLatin1String("dhcp")) {
        d->address.clear();
        d->netmask.clear();
        d->gateway.clear();
    }
}

QString IPV4Data::address() const
{
    Q_D(const IPV4Data);
    return d->address;
}

void IPV4Data::setAddress(const QString &address)
{
    Q_D(IPV4Data);
    if (objectName() == QLatin1String("IPv4.Configuration") &&
           d->method == QLatin1String("dhcp")) {
        qDebug() << Q_FUNC_INFO << "service is configured for dhcp";
        return;
    }

    d->address = address;
}

QString IPV4Data::netmask() const
{
    Q_D(const IPV4Data);
    return d->netmask;
}

void IPV4Data::setNetmask(const QString &netmask)
{
    Q_D(IPV4Data);
    if (objectName() == QLatin1String("IPv4.Configuration") &&
           d->method == QLatin1String("dhcp")) {
        qDebug() << Q_FUNC_INFO << "service is configured for dhcp";
        return;
    }

    d->netmask = netmask;
}

QString IPV4Data::gateway() const
{
    Q_D(const IPV4Data);
    return d->gateway;
}

void IPV4Data::setGateway(const QString &gateway)
{
    Q_D(IPV4Data);
    if (objectName() == QLatin1String("IPv4.Configuration") &&
           d->method == QLatin1String("dhcp")) {
        qDebug() << Q_FUNC_INFO << "service is configured for dhcp";
        return;
    }

    d->gateway = gateway;
}

IPV6Data::IPV6Data(ConnManObject *parent)
    : ConfigurableObject(parent),
      d_ptr(new IPV6DataPrivate)
{
}

IPV6Data::~IPV6Data()
{
}

QString IPV6Data::method() const
{
    Q_D(const IPV6Data);
    return d->method;
}

void IPV6Data::setMethod(const QString &method)
{
    Q_D(IPV6Data);
    d->method = method;
}

QString IPV6Data::address() const
{
    Q_D(const IPV6Data);
    return d->address;
}

void IPV6Data::setAddress(const QString &address)
{
    Q_D(IPV6Data);
    d->address = address;
}

QString IPV6Data::prefixLength() const
{
    Q_D(const IPV6Data);
    return d->prefixLength;
}

void IPV6Data::setPrefixLength(const QString &prefixLength)
{
    Q_D(IPV6Data);
    d->prefixLength = prefixLength;
}

QString IPV6Data::gateway() const
{
    Q_D(const IPV6Data);
    return d->gateway;
}

void IPV6Data::setGateway(const QString &gateway)
{
    Q_D(IPV6Data);
    d->gateway = gateway;
}

QString IPV6Data::privacy() const
{
    Q_D(const IPV6Data);
    return d->privacy;
}

void IPV6Data::setPrivacy(const QString &privacy)
{
    Q_D(IPV6Data);
    d->privacy = privacy;
}

ProxyData::ProxyData(ConnManObject *parent)
    : ConfigurableObject(parent),
      d_ptr(new ProxyDataPrivate)
{
}

ProxyData::~ProxyData()
{
}

QString ProxyData::method() const
{
    Q_D(const ProxyData);
    return d->method;
}

void ProxyData::setMethod(const QString &method)
{
    Q_D(ProxyData);
    d->method = method;
}

QString ProxyData::url() const
{
    Q_D(const ProxyData);
    return d->url;
}

void ProxyData::setUrl(const QString &url)
{
    Q_D(ProxyData);
    d->url = url;
}

QStringList ProxyData::servers() const
{
    Q_D(const ProxyData);
    return d->servers;
}

void ProxyData::setServers(const QStringList &servers)
{
    Q_D(ProxyData);
    d->servers = servers;
}

QStringList ProxyData::excludes() const
{
    Q_D(const ProxyData);
    return d->excludes;
}

void ProxyData::setExcludes(const QStringList &excludes)
{
    Q_D(ProxyData);
    d->excludes = excludes;
}

ProviderData::ProviderData(ConnManObject *parent)
    : ConfigurableObject(parent),
      d_ptr(new ProviderDataPrivate)
{
}

ProviderData::~ProviderData()
{
}

QString ProviderData::host() const
{
    Q_D(const ProviderData);
    return d->host;
}

void ProviderData::setHost(const QString &host)
{
    Q_D(ProviderData);
    d->host = host;
}

QString ProviderData::domain() const
{
    Q_D(const ProviderData);
    return d->domain;
}

void ProviderData::setDomain(const QString &domain)
{
    Q_D(ProviderData);
    d->domain = domain;
}

QString ProviderData::name() const
{
    Q_D(const ProviderData);
    return d->name;
}

void ProviderData::setName(const QString &name)
{
    Q_D(ProviderData);
    d->name = name;
}

QString ProviderData::type() const
{
    Q_D(const ProviderData);
    return d->type;
}

void ProviderData::setType(const QString &type)
{
    Q_D(ProviderData);
    d->type = type;
}

QDebug operator<<(QDebug dbg, const Service *service)
{
    if (!service)
        return dbg << "invalid Service";

    dbg.nospace() << "[ " << service->objectPath().path().toLatin1().constData() << " ]" << endl;

    // first properties
    const QMetaObject *mo = service->metaObject();
    int propertyOffset = ConnManObject::staticMetaObject.propertyOffset();
    int propertyCount = ConnManObject::staticMetaObject.propertyOffset() + mo->propertyCount();
    for (int i = propertyOffset; i < propertyCount; ++i) {
        QMetaProperty mp = mo->property(i);
        QVariant data = mp.read(service);
        if (data.isValid() && !data.isNull()) {
            if (data.canConvert<QObject*>())
                dbg.nospace() << "\t" << data.value<ConfigurableObject*>();
            else
                dbg.nospace() << "\t" << mp.name() << " = " << data.toString().toLatin1().constData() << endl;
        }
    }

    return dbg.space();
}

QDebug operator<<(QDebug dbg, const ConfigurableObject *object)
{
    if (!object)
        return dbg << "invalid ConfigurableObject";

    dbg.nospace() << object->objectName().toLatin1().constData() << " = { ";
    const QMetaObject *mo = object->metaObject();
    for (int i = mo->propertyOffset(); i < mo->propertyCount(); ++i) {
      QMetaProperty mp = mo->property(i);
      QVariant data = mp.read(object);

      if (data.isValid() && !data.isNull())
          dbg.nospace() << mp.name() << "=" << data;
    }
    dbg << "}" << endl;

    return dbg.space();
}
