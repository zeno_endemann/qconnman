TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += src \
           examples

equals(QT_MAJOR_VERSION, 5): SUBDIRS += quick
